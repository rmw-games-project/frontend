export interface IUser {
	id: number;
	name: string;
	email: string;
	photoUrl: string;
	status: string;
	color: string;
	role: Role;
}

export enum Role {
	Admin,
	Player,
}

export const mapRole = (role: any): string => {
	switch (role) {
		case Role.Admin:
			return 'Admin';
		case Role.Player:
			return 'Player';
	}
};
