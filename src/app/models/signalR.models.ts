export type SignalRCommonMethodName =
	| 'onConnected'
	| 'onMessage'
	| 'onError'
	| 'onDisconnected';

export type SignalREventRegistrators = {
	[key in SignalRCommonMethodName]: EventRegistrator;
};

export type SignalREventCallback = (value) => void;
export type EventRegistrator = (cb: SignalREventCallback) => SignalREventRegistrators;
