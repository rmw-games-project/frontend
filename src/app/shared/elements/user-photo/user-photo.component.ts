import { Component, Input, OnInit } from '@angular/core';


@Component({
	selector: 'app-user-photo',
	templateUrl: './user-photo.component.html',
	styleUrls: ['./user-photo.component.scss']
})
export class UserPhotoComponent implements OnInit {
	
	@Input() photoUrl: string;
	@Input() name: string;
	@Input() size = 50;
	@Input() shape: 'round' | 'square' = 'square';
	@Input() description: string;
	@Input() bottomLine: string;
	
	constructor() {
	}
	
	ngOnInit() {
	}
	
	get imageUrl(): string {
		if (this.photoUrl) {
			return 'data:image/jpeg;base64,' + this.photoUrl;
		} else {
			return 'assets/general/images/default-user.png';
		}
	}
}
