import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IUser } from '../../../models/user.models';
import { Session } from '../../../features/game-wrapper/game.interfaces';


@Component({
	selector: 'app-session-item',
	templateUrl: './session-item.component.html',
	styleUrls: ['./session-item.component.scss']
})
export class SessionItemComponent implements OnInit {
	
	@Input() session: Session;
	@Input() user: IUser;
	
	@Output() joinClicked = new EventEmitter<Session>();
	@Output() updateSession = new EventEmitter<Partial<Session>>();
	
	constructor() {
	}
	
	ngOnInit() {
	}
	
	joinGame() {
		this.joinClicked.emit(this.session);
	}
	
	get conditionToDisableJoin(): boolean {
		if (this.session.gameMode === 'multiplayer') {
			return !this.session.playerIds.includes(this.user.id) && this.session.playerIds.length >= this.session.maxParticipants;
		} else {
			return false;
		}
	}
}


