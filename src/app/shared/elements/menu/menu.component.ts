import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
	selector: 'app-menu',
	templateUrl: './menu.component.html',
	styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
	
	@Input() title: string;
	@Input() options: MenuOption[];
	
	@Output() optionSelected = new EventEmitter<MenuOption>();
	
	constructor() {
	}
	
	ngOnInit() {
	}
	
	chooseOption(option: MenuOption) {
		this.optionSelected.emit(option);
	}
}


export interface MenuOption {
	capture: string;
	value: any;
	disabled?: boolean;
}
