import { NgModule } from '@angular/core';
import { DragDropModule } from '@angular/cdk/drag-drop';
import {
	MatButtonModule, MatCheckboxModule,
	MatDialogModule, MatFormFieldModule, MatInputModule,
	MatProgressSpinnerModule, MatSelectModule, MatTableModule,
	MatTabsModule,
	MatTooltipModule
} from '@angular/material';
import { UserPhotoComponent } from './elements/user-photo/user-photo.component';
import { CommonModule } from '@angular/common';
import { LoaderComponent } from './elements/loader/loader.component';
import { BoxComponent } from './elements/box/box.component';
import { MenuComponent } from './elements/menu/menu.component';
import { SessionItemComponent } from './elements/session-item/session-item.component';


const materialModules = [
	DragDropModule,
	MatTooltipModule,
	MatTabsModule,
	MatDialogModule,
	MatButtonModule,
	MatProgressSpinnerModule,
	MatFormFieldModule,
	MatInputModule,
	MatTableModule,
	MatCheckboxModule,
	MatSelectModule,
];

const components = [
	UserPhotoComponent,
	LoaderComponent,
	BoxComponent,
	MenuComponent,
	SessionItemComponent,
];


@NgModule({
	imports: [
		CommonModule,
		...materialModules,
	],
	declarations: [
		...components,
	],
	exports: [
		...materialModules,
		...components,
	],
})
export class SharedModule {}
