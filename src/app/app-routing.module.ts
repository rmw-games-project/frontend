import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './features/auth/auth.component';
import { UnauthorizedGuard } from './features/auth/unauthorized.guard';
import { GameListComponent } from './features/game-list/game-list.component';
import { AuthorizedGuard } from './features/auth/authorized.guard';
import { GameWrapperComponent } from './features/game-wrapper/game-wrapper.component';


const routes: Routes = [
	{
		path: 'auth',
		canActivate: [UnauthorizedGuard],
		component: AuthComponent,
	},
	{
		path: 'games',
		canActivate: [AuthorizedGuard],
		loadChildren: () => import('./features/game-list/game-list.module').then(m => m.GameListModule),
	},
	{
		path: 'game',
		canActivate: [AuthorizedGuard],
		loadChildren: () => import('./features/game-wrapper/game-wrapper.module').then(m => m.GameWrapperModule),
	},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
