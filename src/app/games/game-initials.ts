import { SapperComponent } from './sapper/sapper.component';
import { IGameInitial } from '../features/game-wrapper/game.interfaces';

export const gameInitials: IGameInitial[] = [
	{
		name: 'sapper',
		component: SapperComponent
	},
];
