import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AuthComponent } from './features/auth/auth.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared.module';
import { MaxLengthPipe } from './pipes/max-length.pipe';
import { HeaderComponent } from './features/header/header.component';
import { AuthorizedGuard } from './features/auth/authorized.guard';
import { UserProfileComponent } from './features/user-profile/user-profile.component';
import { TokenInterceptor } from './services/http.interceptor';
import { AuthService } from './features/auth/auth.service';
import { HttpErrorHandler } from './services/http-error-handler';
import { UnauthorizedGuard } from './features/auth/unauthorized.guard';
import { UsersListComponent } from './features/users-list/users-list.component';
import { NotifierModule } from 'angular-notifier';
import { notifierConfig } from '../assets/configs/notifier/notifier.config';
import { ChatModule } from './features/chat/chat.module';
import { SignalRService } from './services/signalR.service';
import { SessionListComponent } from './features/session-list/session-list.component';
import { GamesService } from './services/games.service';


@NgModule({
	entryComponents: [
		UserProfileComponent,
	],
	declarations: [
		AppComponent,
		AuthComponent,
		MaxLengthPipe,
		HeaderComponent,
		UserProfileComponent,
		UsersListComponent,
		SessionListComponent,
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		ReactiveFormsModule,
		BrowserAnimationsModule,
		SharedModule,
		ChatModule,
		
		// Third-party
		NotifierModule.withConfig(notifierConfig),
	],
	providers: [
		AuthorizedGuard,
		UnauthorizedGuard,
		SignalRService,
		GamesService,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: TokenInterceptor,
			multi: true,
			deps: [AuthService],
		},
		{
			provide: HTTP_INTERCEPTORS,
			useClass: HttpErrorHandler,
			multi: true,
		},
	],
	bootstrap: [AppComponent],
})
export class AppModule {}

