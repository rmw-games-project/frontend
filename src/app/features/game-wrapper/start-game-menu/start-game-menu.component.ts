import {
	Component,
	ComponentFactoryResolver, ComponentRef,
	EventEmitter, OnDestroy,
	OnInit,
	Output,
	ViewChild,
	ViewContainerRef
} from '@angular/core';
import {
	GameDataForLaunching,
	IGameInitial,
	GameMode,
	MultiModeAction, Session,
	SingleModeAction, IGame
} from '../game.interfaces';
import { MultiGameSettings } from './create-new-multi-game/multi-game-setting.model';
import { Subscription } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';
import { CreateNewMultiGameComponent } from './create-new-multi-game/create-new-multi-game.component';
import { EnterIdComponent } from './enter-id/enter-id.component';
import { IUser } from '../../../models/user.models';
import { SessionListComponent } from '../../session-list/session-list.component';
import { MenuOption } from '../../../shared/elements/menu/menu.component';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { AuthService } from '../../auth/auth.service';
import { GamesService } from '../../../services/games.service';
import { gameInitials } from '../../../games/game-initials';


@Component({
	selector: 'app-start-game-menu',
	templateUrl: './start-game-menu.component.html',
	styleUrls: ['./start-game-menu.component.scss']
})
export class StartGameMenuComponent implements OnInit, OnDestroy {
	
	@ViewChild('componentEntry', { read: ViewContainerRef, static: false })
	componentEntry: ViewContainerRef;
	
	gameInitials: IGameInitial;
	// gameInfo: GameInfoState;
	
	gameMode: GameMode;
	singleModeAction: SingleModeAction;
	multiModeAction: MultiModeAction;
	multiGameSettings: MultiGameSettings;
	
	subscriptions: Subscription[] = [];
	
	choseModeOptions: MenuOption[] = [
		{value: 'single', capture: 'Одиночный'},
		{value: 'multiplayer', capture: 'Многопользовательский', disabled: true}
	];
	choseSingleModeActions: MenuOption[] = [
		{value: 'newGame', capture: 'Новая игра'},
		{value: 'continueLast', capture: 'Продолжить последнюю'},
		{value: 'continue', capture: 'Продолжить игру'},
		{value: 'watchSavedGames', capture: 'Смотреть сохраненные игры', disabled: true}
	];
	choseMultiModeActions: MenuOption[] = [
		{value: 'createNewGame', capture: 'Создать новую игру'},
		{value: 'joinGameById', capture: 'Присоединиться по ID'},
		{value: 'joinGame', capture: 'Присоединиться'},
		{value: 'watchSavedGames', capture: 'Смотреть сохраненные игры'}
	];
	
	@Output() gameDataPrepared = new EventEmitter<GameDataForLaunching>();
	
	constructor(
		private resolver: ComponentFactoryResolver,
		private route: ActivatedRoute,
		private authService: AuthService,
		private gamesService: GamesService,
	) {
		this.subscribeToRequiredData();
	}
	
	subscribeToRequiredData(): void {
		console.log('this.route.snapshot:', this.route.snapshot.params);
		this.gamesService
			.getGames()
			.subscribe((games: IGame[]) => {
				this.gameInitials = gameInitials.find(gameInitial => {
					return gameInitial.name === this.route.snapshot.params.name;
				});
			});
		
		// this.subscriptions = [
		//
		// 	this.store.select(selectGameNameFromParams).pipe(
		// 		filter(gameName => !!gameName)
		// 	).subscribe((gameName) => {
		//
		// 		this.gameInitials = GAMES.find(gameInitial => gameInitial.name === gameName);
		//
		// 		this.firestoreService.getGameIdByName(gameName).subscribe((id: string) => {
		// 			this.store.dispatch(new SetGameInfo({id: id, name: gameName}));
		// 		});
		// 	}),

		// 	this.store.select(selectGameInfoState)
		// 		.subscribe((gameInfoState: GameInfoState) => {
		// 			this.gameInfo = gameInfoState;
		// 		})
		// ];
	}
	
	ngOnInit() {
		this.prepareMenu();
	}
	
	gameModeSelected(gameMode: GameMode) {
		this.gameMode = gameMode;
		// this.store.dispatch(new SetGameInfo({gameMode: this.gameMode}));
	}
	
	runGame(specificGameDetails) {
		this.createSession(specificGameDetails);
		this.emitPreparedGameData();
	}
	
	singleModeActionSelected(singleModeAction: SingleModeAction) {
		this.singleModeAction = singleModeAction;
		
		switch (this.singleModeAction) {
			case 'newGame':
				if (this.gameInitials.menuComponent) {
					this.showGameMenu();
				} else {
					this.runGame({});
				}
				break;
			case 'continue':
				this.showSessionList();
				break;
		}
	}
	
	showGameMenu() {
		const gameMenuRef = this.createComponent(this.gameInitials.menuComponent);
		
		this.subscriptions.push(
			gameMenuRef.instance.menuClosed.subscribe((specificGameDetails) => {
				gameMenuRef.destroy();
				this.runGame(specificGameDetails);
			})
		);
	}
	
	showSessionList(): void {
		const sessionListRef = this.createComponent(SessionListComponent);
		
		// let query: Query;
		// if (this.gameMode === 'single') {
		// 	query = {
		// 		where: [
		// 			{field: 'isSessionOver', opStr: '==', value: false},
		// 			{field: 'gameMode', opStr: '==', value: 'single'},
		// 			{field: 'creator.uid', opStr: '==', value: this.user.uid}
		// 		]
		// 	};
		// } else {
		// 	query = {
		// 		where: [
		// 			{field: 'isSessionOver', opStr: '==', value: false},
		// 			{field: 'gameMode', opStr: '==', value: 'multiplayer'},
		// 			{field: 'private', opStr: '==', value: false}
		// 		]
		// 	};
		// }
		
		// this.store.dispatch(new SubscribeToSessionList(query));
		// this.subscriptions.push(this.store.select(selectSessionListLoaded).pipe(
		// 	filter((loaded: boolean) => !!loaded),
		// 	switchMap(() => this.store.select(selectSessionListAll))
		// ).subscribe((sessionList: Session[]) => {
		// 	sessionListRef.instance.sessions = sessionList;
		// 	sessionListRef.instance.user = this.user;
		// }));
		
		// this.subscriptions.push(sessionListRef.instance.sessionChosen.subscribe((session: Session) => {
		// 	sessionListRef.destroy();
		// 	this.store.dispatch(new UnsubscribeFromSessionList());
		//
		// 	if (this.gameMode === 'multiplayer') {
		// 		if (!session.playerIds.includes(this.user.uid)) {
		// 			session.playerIds.push(this.user.uid);
		// 			this.store.dispatch(new AddPlayer(this.user));
		// 			this.store.dispatch(new UpdateSession({playerIds: session.playerIds, id: session.id}));
		// 		}
		// 		this.store.dispatch(new SubscribeToSession({id: session.id}));
		// 	} else if (this.gameMode === 'single') {
		// 		this.store.dispatch(new SetSession(session));
		// 	}
		// 	this.store.dispatch(new LoadSteps({sessionId: session.id}));
		//
		// 	this.emitPreparedGameData();
		// }));
	}
	
	multiModeActionSelected(multiModeAction: MultiModeAction) {
		this.multiModeAction = multiModeAction;
		
		switch (this.multiModeAction) {
			case 'createNewGame':
				this.showCreateNewMultiGameMenu();
				break;
			case 'joinGameById':
				this.showEnterIdComponent();
				break;
			case 'joinGame':
				this.showSessionList();
		}
	}
	
	showEnterIdComponent() {
		const componentRef = this.createComponent(EnterIdComponent);
		
		this.subscriptions.push(
			componentRef.instance.sessionFound.subscribe((session: Session) => {
				componentRef.destroy();
				
				// this.store.dispatch(new SetSession(session));
				// this.store.dispatch(new LoadSteps({sessionId: session.id}));
				this.emitPreparedGameData();
			})
		);
	}
	
	showCreateNewMultiGameMenu() {
		const componentRef = this.createComponent(CreateNewMultiGameComponent);
		
		// componentRef.instance.preConfig = this.gameInitials.startGameConfig.multiplayerMode.multiModeConfig;
		this.subscriptions.push(
			componentRef.instance.multiSettingsChosen.subscribe((multiGameSettings: MultiGameSettings) => {
				componentRef.destroy();
				this.multiGameSettings = multiGameSettings;
				
				if (this.gameInitials.menuComponent) {
					this.showGameMenu();
				} else {
					this.runGame({});
				}
			})
		);
	}
	
	createSession(specificGameDetails) {
		const payload: Session = this.generateDataForCreatedSession(specificGameDetails);
		// this.store.dispatch(new CreateSession(payload));
	}
	
	createComponent(component): ComponentRef<any> {
		const factory = this.resolver.resolveComponentFactory(component);
		return this.componentEntry.createComponent(factory);
	}
	
	generateDataForCreatedSession(createdGameData: any): Session {
		// const newSession: Session = {
		// 	created: this.firestoreService.getFirestoreTimestamp(),
		// 	creator: {
		// 		id: this.user.uid,
		// 		photoUrl: this.user.photoURL,
		// 		name: this.user.name
		// 	},
		// 	gameMode: this.gameMode,
		// 	gameData: createdGameData,
		// 	isSessionOver: false
		// };
		//
		// if (newSession.gameMode === 'multiplayer') {
		// 	newSession.private = this.multiGameSettings.private;
		// 	newSession.maxParticipants = this.multiGameSettings.maxParticipants;
		// 	newSession.moveOrder = this.multiGameSettings.moveOrder;
		// 	newSession.playerIds = [this.user.uid];
		// }
		//
		// return newSession;
		return null;
	}
	
	emitPreparedGameData() {
		this.gameDataPrepared.emit({
			gameMode: this.gameMode,
			action: this.gameMode === 'single' ? this.singleModeAction : this.multiModeAction,
			gameComponent: this.gameInitials.component,
			user: this.authService.user,
		});
	}
	
	prepareMenu() {
		// this.choseModeOptions[0].disabled = this.gameInitials.startGameConfig.singleMode.disabled;
		// this.choseModeOptions[1].disabled = this.gameInitials.startGameConfig.multiplayerMode.disabled;
		//
		// this.choseSingleModeActions[0].disabled = this.gameInitials.startGameConfig.singleMode.disabled;
		// this.choseSingleModeActions[1].disabled = this.gameInitials.startGameConfig.singleMode.continueLastDisabled;
		// this.choseSingleModeActions[2].disabled = this.gameInitials.startGameConfig.singleMode.continueDisabled;
		// this.choseSingleModeActions[3].disabled = this.gameInitials.startGameConfig.singleMode.watchSavedGamesDisabled;
		//
		// this.choseMultiModeActions[0].disabled = this.gameInitials.startGameConfig.multiplayerMode.createNewDisabled;
		// this.choseMultiModeActions[1].disabled = this.gameInitials.startGameConfig.multiplayerMode.joinGameByIdDisabled;
		// this.choseMultiModeActions[2].disabled = this.gameInitials.startGameConfig.multiplayerMode.joinGameDisabled;
		// this.choseMultiModeActions[3].disabled = this.gameInitials.startGameConfig.multiplayerMode.watchSavedGamesDisabled;
	}
	
	ngOnDestroy(): void {
		this.subscriptions.forEach(sub => sub.unsubscribe());
	}
}
