import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameWrapperComponent } from './game-wrapper.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { StartGameMenuComponent } from './start-game-menu/start-game-menu.component';
import { EnterIdComponent } from './start-game-menu/enter-id/enter-id.component';
import { CreateNewMultiGameComponent } from './start-game-menu/create-new-multi-game/create-new-multi-game.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SapperComponent } from '../../games/sapper/sapper.component';
import { SapperStartMenuComponent } from '../../games/sapper/sapper-start-menu/sapper-start-menu.component';
import { CustomFieldComponent } from '../../games/sapper/custom-field/custom-field.component';
// import { StartGameMenuComponent } from './start-game-menu/start-game-menu.component';


const ROUTES: Routes = [
	{
		path: '',
		component: GameWrapperComponent
	},
	{
		path: ':game',
		component: GameWrapperComponent
	}
];


const gamesComponents = [
	CustomFieldComponent,
	// TicTacToeComponent,
	SapperComponent,
	// ChessComponent,
	SapperStartMenuComponent,
	// SessionListComponent,
	CreateNewMultiGameComponent,
	EnterIdComponent,
	// ChessPieceComponent,
	// ChoosePieceComponent,
];


@NgModule({
	declarations: [
		// SessionItemComponent,
		GameWrapperComponent,
		// MenuComponent,
		StartGameMenuComponent,
		// PlayerListComponent,
		...gamesComponents,
		// PlayerComponent
	],
	imports: [
		CommonModule,
		SharedModule,
		RouterModule.forChild(ROUTES),
		ReactiveFormsModule,
	],
	entryComponents: [
		...gamesComponents
	],
	providers: []
})
export class GameWrapperModule {
}
