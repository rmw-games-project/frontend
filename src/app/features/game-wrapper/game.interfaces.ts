import { StartGameConfig } from './start-game-menu/start-game-menu.interface';
import { GameMoveOrder } from './start-game-menu/create-new-multi-game/multi-game-setting.model';
import { IUser } from '../../models/user.models';

export interface IGame {
	id: string;
	name: string;
	routeName: string;
	image: string;
	isBlocked: boolean;
}

export type GameList = IGame[];

export interface IGameInitial {
	name: string;
	// imageSrc: string;
	component: any;
	menuComponent?: any;
	// startGameConfig: StartGameConfig;
}


export type GameMode = 'single' | 'multiplayer';
export type SingleModeAction = 'newGame' | 'continue' | 'continueLast' | 'watchSavedGames';
export type MultiModeAction = 'createNewGame' | 'joinGameById' | 'joinGame' | 'watchSavedGames';


export interface Session<T = any> {
	id?: string;
	
	// For single and multi modes
	isSessionOver: boolean;
	created: any;
	creator: Partial<IUser>;
	gameData: T;
	gameMode: GameMode;
	
	// For multi mode only
	private?: boolean;
	maxParticipants?: number;
	moveOrder?: GameMoveOrder;
	playerIds?: number[];
}

export interface Step<T = any> {
	id?: number;
	userId: number;
	timestamp?: any;
	data: T;
}

export interface GameDataForLaunching {
	gameMode: GameMode;
	action: SingleModeAction | MultiModeAction;
	gameComponent: any;
	user: IUser;
}

export interface Player {
	uid: string;
	name: string;
	email: string;
	online: string;
	photoURL: string;
}
