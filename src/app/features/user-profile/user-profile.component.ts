import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { emersionAnimation } from '../../animations/emersion.animation';
import { IUser, Role } from '../../models/user.models';
import { UsersService } from '../../services/users.service';


@Component({
	selector: 'app-user-profile',
	templateUrl: './user-profile.component.html',
	styleUrls: ['./user-profile.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	animations: [emersionAnimation],
})
export class UserProfileComponent implements OnInit {
	
	readonly NAME_MIN_LENGTH = 2;
	readonly NAME_MAX_LENGTH = 20;
	readonly EMAIL_MIN_LENGTH = 6;
	
	@Input() editable: boolean;
	
	form: FormGroup = this.fb.group({
		name: ['', [Validators.required, Validators.minLength(this.NAME_MIN_LENGTH), Validators.maxLength(this.NAME_MAX_LENGTH)]],
		email: ['', [Validators.required, Validators.minLength(6), Validators.email]],
		color: ['']
	});
	
	user: IUser;
	
	constructor(
		private fb: FormBuilder,
		private usersService: UsersService,
		private cdr: ChangeDetectorRef,
		private dialogRef: MatDialogRef<UserProfileComponent>,
		@Inject(MAT_DIALOG_DATA) public data: { user: IUser, editable: boolean }
	) {}
	
	ngOnInit() {
		this.user = this.data.user;
		this.updateForm();
	}
	
	updateForm() {
		if (this.data.editable) {
			this.form.enable();
		} else {
			this.form.disable();
		}
		
		this.form.patchValue({
			name: this.user.name,
			email: this.user.email,
			color: this.user.color || 'black',
		});
	}
	
	saveProfile() {
		this.dialogRef.close({
			...this.form.value,
			id: this.user.id,
		});
	}
	
	get emailErrorMessage() {
		const email = this.form.get('email');
		return email.hasError('required') ? 'Обязательное поле' :
			email.hasError('email') ? 'Неверный e-mail' :
				email.hasError('minlength') ? 'Минимальная длина' + ' - ' + this.EMAIL_MIN_LENGTH : '';
	}
}
