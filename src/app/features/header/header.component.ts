import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { fromEvent, Observable, Subscription } from 'rxjs';
import { MatDialog } from '@angular/material';
import { IUser } from '../../models/user.models';
import { AuthService } from '../auth/auth.service';
import { UserProfileComponent } from '../user-profile/user-profile.component';
import { UsersService } from '../../services/users.service';
import { filter } from 'rxjs/operators';


@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent implements OnInit, OnDestroy {
	
	@ViewChild('userMenu', { static: false })
	userMenu: ElementRef;
	
	subs: Subscription[] = [];
	userMenuOpened: boolean;
	
	user$: Observable<IUser> = this.authService.user$;
	
	constructor(
		private modal: MatDialog,
		private authService: AuthService,
		private usersService: UsersService,
		private cdr: ChangeDetectorRef,
	) {}
	
	openUserMenu() {
		this.userMenuOpened = true;
		setTimeout(() => this.subscribeToClicks());
	}
	
	closeUserMenu() {
		this.userMenuOpened = false;
		this.unsubscribe();
	}
	
	ngOnInit() {}
	
	subscribeToClicks() {
		this.subs.push(
			fromEvent(window, 'click')
				.subscribe(() => {
					this.closeUserMenu();
					this.cdr.detectChanges();
				})
		);
		
		this.subs.push(
			fromEvent(this.userMenu.nativeElement, 'click')
				.subscribe((event: any) => event.stopPropagation())
		);
	}
	
	singOut() {
		this.authService.logout();
	}
	
	goToUserProfile() {
		this.modal
			.open(UserProfileComponent, {
				data: { editable: true, user: this.authService.user },
			})
			.afterClosed()
			.pipe(filter(Boolean))
			.subscribe((data: Partial<IUser>) => {
				this.usersService
					.updateUser(data)
					.subscribe((user: IUser) => {
						this.authService.updateUser(user);
					});
			});
		
		this.closeUserMenu();
	}
	
	unsubscribe() {
		this.subs.forEach((sub) => sub.unsubscribe());
	}
	
	ngOnDestroy() {
		this.unsubscribe();
	}
	
	async onFileInput(event) {
		const file: File = event.target.files[0];
		const userId = this.authService.user.id;
		await this.usersService.updatePhoto(userId, file).toPromise();
		this.authService.updateUser();
	}
}
