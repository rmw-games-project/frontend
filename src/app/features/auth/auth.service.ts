import { Injectable } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { IAuthData, IAuthResponse } from './auth.models';
import { BehaviorSubject } from 'rxjs';
import { IUser } from '../../models/user.models';
import { Router } from '@angular/router';
import jwtDecode from 'jwt-decode';
import { UsersService } from '../../services/users.service';


@Injectable({
	providedIn: 'root'
})
export class AuthService {
	
	user$ = new BehaviorSubject<IUser>(null);
	isLoggedIn$ = new BehaviorSubject<boolean>(null);
	
	constructor(
		private apiService: ApiService,
		private router: Router,
		private usersService: UsersService,
	) {}
	
	register(data: IAuthData) {
		this.apiService
			.post('auth/register', data)
			.subscribe((response: IAuthResponse) => {
				this.authenticate(response);
				this.router.navigateByUrl('');
			});
	}
	
	login(data: IAuthData) {
		this.apiService
			.post('auth/login', data)
			.subscribe((response: IAuthResponse) => {
				this.authenticate(response);
				this.router.navigateByUrl('');
			});
	}
	
	logout() {
		this.isLoggedIn$.next(false);
		this.user$.next(null);
		this.clearToken();
		this.router.navigateByUrl('/auth');
	}
	
	updateUser(user?: IUser) {
		if (user) {
			this.user$.next(user);
		} else {
			this.usersService
				.requestUser(this.user.id)
				.subscribe((iUser: IUser) => {
					this.user$.next(iUser);
				});
		}
	}
	
	private authenticate({ user, token }: IAuthResponse) {
		this.isLoggedIn$.next(true);
		this.user$.next(user);
		this.setToken(token);
	}
	
	public markAsUnauthorized() {
		this.user$.next(null);
		this.isLoggedIn$.next(false);
	}
	
	getToken(): string {
		return localStorage.getItem('token');
	}
	
	private setToken(token: string) {
		localStorage.setItem('token', token);
	}
	
	private clearToken() {
		localStorage.removeItem('token');
	}
	
	get user() {
		return this.user$.value;
	}
	
	public get isLoggedIn(): boolean {
		return this.isLoggedIn$.value;
	}
	
	checkIsLoggedIn() {
		const token = localStorage.getItem('token');
		if (!token) {
			this.markAsUnauthorized();
			return;
		}
		const { id } = jwtDecode(token);
		this.usersService
			.requestUser(id)
			.subscribe((user: IUser) => {
				if (user) {
					this.user$.next(user);
					this.isLoggedIn$.next(true);
				} else {
					this.markAsUnauthorized();
				}
			});
	}
}
