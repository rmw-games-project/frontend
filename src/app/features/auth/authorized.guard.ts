import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { filter, mapTo } from 'rxjs/operators';


@Injectable()
export class AuthorizedGuard implements CanActivate {
	
	constructor(
		private authService: AuthService,
	) {}
	
	canActivate(): Observable<boolean> {
		return this.authService.isLoggedIn$.pipe(
			filter((isLoggedId) => isLoggedId !== null),
		);
	}
}
