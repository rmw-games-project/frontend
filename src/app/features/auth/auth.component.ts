import { Component, ChangeDetectionStrategy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
	selector: 'app-auth',
	templateUrl: './auth.component.html',
	styleUrls: ['./auth.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AuthComponent {
	
	isRegisterMode = false;
	
	authForm: FormGroup = this.fb.group({
		email: ['', [Validators.required, Validators.email]],
		password: ['', [Validators.required, Validators.pattern(/\w{3,}/)]],
	});
	
	constructor(
		private http: HttpClient,
		private authService: AuthService,
		private fb: FormBuilder,
	) {}
	
	onSubmit() {
		if (this.authForm.invalid) {
			return;
		}
		if (this.isRegisterMode) {
			this.authService.register(this.authForm.value);
		} else {
			this.authService.login(this.authForm.value);
		}
	}
}
