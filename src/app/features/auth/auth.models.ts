import { IUser } from '../../models/user.models';

export interface IAuthData {
	email: string;
	password: string;
}

export interface IAuthResponse {
	token: string;
	user: IUser;
}
