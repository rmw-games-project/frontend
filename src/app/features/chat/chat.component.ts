import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { IMessage } from './message/message.models';
import { Subject } from 'rxjs';
import { NotifierService } from 'angular-notifier';
import { MaxLengthPipe } from '../../pipes/max-length.pipe';
import { IUser } from '../../models/user.models';
import { AuthService } from '../auth/auth.service';
import { ChatService } from './chat.service';
import { takeUntil } from 'rxjs/operators';
import { generalTab, IChatTab } from './chat.models';


@Component({
	selector: 'app-chat',
	templateUrl: './chat.component.html',
	styleUrls: ['./chat.component.scss'],
	providers: [MaxLengthPipe, ChatService],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChatComponent implements OnInit, OnDestroy {
	
	@ViewChild('chatContainer', { static: true })
	chatContainer: ElementRef;
	
	chatHidden = false;
	
	user: IUser;
	messages: IMessage[];
	amountUnreadMessages = 0;
	waitingMessage: Partial<IMessage>;
	
	tabs: IChatTab[] = [
		generalTab,
	];
	
	activeTab: IChatTab;
	
	scrollChatToBottom$ = new Subject<boolean>();
	
	destroyed$ = new Subject<void>();
	Boolean = Boolean;
	
	constructor(
		private notifierService: NotifierService,
		private maxLengthPipe: MaxLengthPipe,
		private authService: AuthService,
		public chatService: ChatService,
		private cdr: ChangeDetectorRef,
	) {}
	
	ngOnInit() {
		this.chatService.connectToChat();
		this.subscribeToUser();
		this.changeTab(0);
	}
	
	subscribeToUser() {
		this.authService
			.user$
			.pipe(takeUntil(this.destroyed$))
			.subscribe((user: IUser) => {
				this.user = user;
			});
	}
	
	async changeTab(tabIndex: number) {
		const tab: IChatTab = this.tabs[tabIndex];
		this.chatService.setCurrentTabSessionId(tab.sessionId);
		this.activeTab = tab;
		this.waitingMessage = null;
		this.getMessages(tab);
	}
	
	async getMessages(tab: IChatTab) {
		this.messages = await this.chatService.getInitialMessages(tab.sessionId).toPromise();
		this.chatService.newMessage$
			.subscribe((message: IMessage) => {
				if (this.waitingMessage) {
					this.waitingMessage.sessionId = message.sessionId;
					this.waitingMessage.timestamp = message.timestamp;
					this.waitingMessage.id = message.id;
					this.waitingMessage = null;
					this.messages = this.messages.slice();
				} else {
					this.messages = [...this.messages, message];
				}
				
				if (this.chatHidden) {
					// TODO when will second chat fix it
					this.amountUnreadMessages++;
					const shortenedMessageText = this.maxLengthPipe.transform(message.text, 45);
					const note = `${message.name || 'Anonymous'}: ${shortenedMessageText}`;
					this.notifierService.notify('default', note);
				}
				
				this.cdr.detectChanges();
			});
		this.cdr.detectChanges();
	}
	
	async loadMoreMessages() {
		this.messages = await this.chatService.loadMoreMessages();
		this.cdr.detectChanges();
	}
	
	hideChat() {
		const transformStyle = this.chatContainer.nativeElement.style.transform;
		let xCutStyle;
		let transformXNum;
		let yCutStyle;
		let transformYNum;
		
		if (transformStyle) {
			xCutStyle = transformStyle.substr(12);
			transformXNum = parseInt(xCutStyle, 10);
			const indexToCut = xCutStyle.indexOf(',') + 1;
			yCutStyle = xCutStyle.substr(indexToCut);
			transformYNum = parseInt(yCutStyle, 10);
		} else {
			transformXNum = 0;
			transformYNum = 0;
		}
		
		this.chatHidden = true;
		this.chatContainer.nativeElement.style.right = `${-320 + transformXNum}px`;
		this.chatContainer.nativeElement.style.bottom = `${(window.innerHeight + transformYNum) / 2 - (535 / 2)}px`;
	}
	
	showChat() {
		this.chatHidden = false;
		setTimeout(() => this.amountUnreadMessages = 0, 800);
		this.chatContainer.nativeElement.style.right = `${20}px`;
	}
	
	sendMessage(messageText: string) {
		const message: any = {
			text: messageText,
			name: this.user.name,
			userId: this.user.id,
		};
		
		this.waitingMessage = message;
		this.messages = [...this.messages, message];
		this.chatService.sendMassage(messageText);
		this.scrollChatToBottom$.next();
	}
	
	get chatTooltip(): string {
		return `Показать чат | Непрочитанных сообщений: ${this.amountUnreadMessages}`;
	}
	
	ngOnDestroy() {
		this.destroyed$.next();
		this.destroyed$.complete();
	}
}
