import { Injectable, OnDestroy } from '@angular/core';
import { SignalRService } from '../../services/signalR.service';
import { HubConnection } from '@microsoft/signalr';
import { BehaviorSubject, Subject } from 'rxjs';
import { IMessage } from './message/message.models';
import { generalTab } from './chat.models';
import { filter, map, take } from 'rxjs/operators';


@Injectable()
export class ChatService implements OnDestroy {
	
	readonly amountMoreMessages = 5;
	private currentTabSessionId: number;
	private connection: HubConnection;
	private messages: { [name: number]: BehaviorSubject<IMessage[]> } = {};
	
	isAllMoreMessagesLoaded: boolean;
	newMessage$ = new Subject<IMessage>();
	showMoreMessagesLoader$ = new BehaviorSubject(null);
	
	constructor(
		private signalRService: SignalRService,
	) {}
	
	connectToChat() {
		this.messages[generalTab.sessionId] = new BehaviorSubject<IMessage[]>(null);
		
		this.signalRService
			.connect('chat', c => this.connection = c)
			.onConnected((messages: IMessage[]) => {
				this.messages[generalTab.sessionId].next(messages);
			})
			.onError(() => {
				console.log('onError');
			})
			.onDisconnected(() => {
				console.log('onDisconnected');
			})
			.onMessage(this.addMessage.bind(this));
		
		this.signalRService.registerEvent(
			'onMoreMessages',
			this.connection,
			(messages: IMessage[]) => {
				if (messages.length) {
					const sessionId = messages[0].sessionId;
					this.getMessages(sessionId).unshift(...messages);
				}
				
				if (messages.length < this.amountMoreMessages) {
					this.isAllMoreMessagesLoaded = true;
				}
			}
		);
	}
	
	sendMassage(message: string) {
		this.connection.invoke('Send', 0, message);
	}
	
	async loadMoreMessages(): Promise<IMessage[]> {
		this.showMoreMessagesLoader$.next(true);
		const firstMessageIndex = this.getMessages(this.currentTabSessionId)[0].id;
		await this.connection.invoke(
			'LoadMoreMessages',
			firstMessageIndex,
			this.currentTabSessionId,
		);
		this.showMoreMessagesLoader$.next(false);
		return this.getMessages(this.currentTabSessionId).slice();
}
	
	getInitialMessages(sessionId: number) {
		return this.messages[sessionId]
			.pipe(
				filter(Boolean),
				map((messages: IMessage[]) => messages.slice()),
				take(1),
			);
	}
	
	setCurrentTabSessionId(id: number) {
		this.currentTabSessionId = id;
		this.isAllMoreMessagesLoaded = false;
	}
	
	private getMessages(sessionId: number): IMessage[] {
		return this.messages[sessionId].value;
	}
	
	private addMessage(message: IMessage) {
		this.getMessages(message.sessionId).push(message);
		if (this.currentTabSessionId === message.sessionId) {
			this.newMessage$.next(message);
		}
	}
	
	ngOnDestroy() {
		this.connection.stop();
	}
}
