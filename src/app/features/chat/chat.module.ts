import { NgModule } from '@angular/core';
import { ChatComponent } from './chat.component';
import { MessageListComponent } from './message-list/message-list.component';
import { MessageComponent } from './message/message.component';
import { InputAreaComponent } from './input-area/input-area.component';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ChatService } from './chat.service';


@NgModule({
	imports: [
		CommonModule,
		SharedModule,
		ReactiveFormsModule,
	],
	declarations: [
		ChatComponent,
		MessageListComponent,
		MessageComponent,
		InputAreaComponent,
	],
	exports: [
		ChatComponent,
	],
	providers: [],
})
export class ChatModule {}
