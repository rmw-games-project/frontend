import { Component, Input, OnInit } from '@angular/core';
import { IMessage } from './message.models';
import { IUser } from '../../../models/user.models';

@Component({
	selector: 'app-message',
	templateUrl: './message.component.html',
	styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {
	
	@Input()
	user: IUser;
	
	@Input()
	message: IMessage;
	
	constructor() {
	}
	
	ngOnInit() {
	}
	
}
