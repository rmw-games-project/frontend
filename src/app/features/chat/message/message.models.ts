export interface IMessage {
	id: string;
	name: string;
	text: string;
	timestamp: any;
	userId: number;
	sessionId: number;
}
