import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
	selector: 'app-input-area',
	templateUrl: './input-area.component.html',
	styleUrls: ['./input-area.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InputAreaComponent implements OnInit {
	
	@Input()
	isThereWaitingMessage: boolean;
	
	@Output()
	send = new EventEmitter<string>();
	
	chatForm: FormGroup = this.fb.group({
		textInput: [''],
	});
	
	constructor(
		private fb: FormBuilder
	) {}
	
	ngOnInit() {}
	
	onSend(text: string) {
		if (this.isThereWaitingMessage) {
			return;
		}
		if (!text || text.trim().length === 0) {
			return;
		}
		this.send.emit(text);
		this.chatForm.reset();
	}
}
