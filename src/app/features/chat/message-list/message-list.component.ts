import {
	AfterViewInit,
	ChangeDetectionStrategy,
	ChangeDetectorRef,
	Component,
	ElementRef, EventEmitter,
	Input,
	OnDestroy,
	OnInit, Output,
	ViewChild
} from '@angular/core';
import { IMessage } from '../message/message.models';
import { Subject } from 'rxjs';
import { emersionAnimation } from '../../../animations/emersion.animation';
import { IUser } from '../../../models/user.models';
import { filter, takeUntil } from 'rxjs/operators';
import { ChatService } from '../chat.service';

@Component({
	selector: 'app-message-list',
	templateUrl: './message-list.component.html',
	styleUrls: ['./message-list.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	animations: [emersionAnimation],
})
export class MessageListComponent implements OnInit, OnDestroy, AfterViewInit {
	
	@Input()
	scrollChatToBottom$: Subject<boolean>;
	
	@Input()
	user: IUser;
	
	@Input()
	set messages(messages: IMessage[]) {
		this._messages = messages;
		if (this.atTheBottom || !this.isAlreadyScrolled) {
			setTimeout(() => this.goToBottom(), 1000);
		}
	}
	
	get messages(): IMessage[] {
		return this._messages;
	}
	
	@Output()
	loadMoreMessages = new EventEmitter<void>();
	
	@ViewChild('messagesWrap', { static: false })
	messagesWrap: ElementRef;
	
	@ViewChild('messagesContainer', { static: false })
	messagesContainer: ElementRef;
	
	_messages: IMessage[];
	
	destroyed$ = new Subject<void>();
	
	showLoader = false;
	atTheBottom: boolean;
	isAlreadyScrolled = false;
	
	lastWrapHeight: number;
	
	constructor(
		private cdr: ChangeDetectorRef,
		private chatService: ChatService,
	) {}
	
	ngOnInit() {
		if (this.scrollChatToBottom$) {
			this.scrollChatToBottom$
				.pipe(takeUntil(this.destroyed$))
				.subscribe(() => {
					setTimeout(() => this.goToBottom());
				});
		}
		
		this.chatService
			.showMoreMessagesLoader$
			.pipe(
				takeUntil(this.destroyed$),
				filter((x) => x !== null)
			)
			.subscribe((showLoader: boolean) => {
				this.showLoader = showLoader;
				if (!showLoader) {
					setTimeout(() => {
						const currentHeight = this.messagesWrap.nativeElement.clientHeight;
						const scrollFromTop = currentHeight - this.lastWrapHeight;
						this.messagesContainer.nativeElement.scroll({
							top: scrollFromTop,
						});
					});
				}
			});
	}
	
	ngAfterViewInit() {
		this.messagesContainer.nativeElement.onscroll = () => {
			this.isAlreadyScrolled = true;
			if (
				this.messagesContainer.nativeElement.scrollTop === 0 &&
				!this.chatService.isAllMoreMessagesLoaded
			) {
				this.lastWrapHeight = this.messagesWrap.nativeElement.clientHeight;
				this.loadMoreMessages.emit();
			}
			const scrollTop = Math.floor(this.messagesContainer.nativeElement.scrollTop);
			const containerHeight = this.messagesContainer.nativeElement.clientHeight;
			const wrapHeight = this.messagesWrap.nativeElement.clientHeight;
			this.atTheBottom = Math.abs(scrollTop + containerHeight - wrapHeight) < 10;
			this.cdr.detectChanges();
		};
	}
	
	goToBottom() {
		if (!this.messagesWrap) {
			return;
		}
		// this.messagesWrap.nativeElement.scrollIntoView(false);
		this.messagesContainer.nativeElement.scroll({
			top: this.messagesWrap.nativeElement.clientHeight,
			behavior: this.isAlreadyScrolled ? 'smooth' : 'auto'
		});
	}
	
	ngOnDestroy() {
		this.destroyed$.next();
		this.destroyed$.complete();
	}
	
	test() {
		console.log(this.messagesWrap.nativeElement.clientHeight);
	}
}
