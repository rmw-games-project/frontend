export interface IChatTab {
	title: string;
	sessionId: number;
}

export const generalTab: IChatTab = {
	title: 'Общий',
	sessionId: 0,
};
