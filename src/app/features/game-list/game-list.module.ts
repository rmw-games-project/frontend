import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { GameListComponent } from './game-list.component';
import { SharedModule } from '../../shared/shared.module';
import { GamesService } from '../../services/games.service';


const ROUTES: Routes = [
	{
		path: '',
		component: GameListComponent
	}
];


@NgModule({
	declarations: [
		GameListComponent
	],
	imports: [
		CommonModule,
		RouterModule.forChild(ROUTES),
		SharedModule,
	],
	providers: [],
})
export class GameListModule {
}
