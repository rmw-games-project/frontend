import { Component, OnInit } from '@angular/core';
import { IGame, GameList } from '../game-wrapper/game.interfaces';
import { map, switchMap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { emersionAnimation } from '../../animations/emersion.animation';
import { GamesService } from '../../services/games.service';


@Component({
	selector: 'app-game-list',
	templateUrl: './game-list.component.html',
	styleUrls: ['./game-list.component.scss'],
	animations: [emersionAnimation],
})
export class GameListComponent implements OnInit {
	
	games$: Observable<IGame[]> = this.gamesService.getGames();
	// games$: Observable<any> = this.store.select(selectGameListLoaded).pipe(
	// 	switchMap((loaded: boolean) => {
	// 		if (!loaded) {
	// 			this.store.dispatch(new LoadGames());
	// 			return of(null);
	// 		} else {
	// 			return this.store.select(selectGameList).pipe(
	// 				map((gamesList: GameList) => {
	// 					return gamesList.filter((gameItem: GameItem) => {
	// 						const names = GAMES.map((game: GameInitial) => game.name);
	// 						return names.includes(gameItem.name);
	// 					});
	// 				}),
	// 				map((gamesList: GameList) => gamesList.filter((game: GameItem) => !game.blocked))
	// 			);
	// 		}
	// 	})
	// );
	
	constructor(
		private gamesService: GamesService,
	) {}
	
	ngOnInit() {
		console.log('kek');
		this.gamesService.getGames().subscribe(console.log);
	}
	
	playClicked(game: IGame) {
		// this.store.dispatch(new RouterGo({path: ['game', `${game.name}`]}));
	}
	
	blockGame(game: IGame) {
		// this.store.dispatch(new UpdateGameItem({
		// 	id: game.id,
		// 	changes: {
		// 		blocked: true
		// 	}
		// }));
	}
}
