import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { emersionAnimation } from '../../animations/emersion.animation';
import { IUser } from '../../models/user.models';
import { Session } from '../game-wrapper/game.interfaces';


@Component({
	selector: 'app-session-list',
	templateUrl: './session-list.component.html',
	styleUrls: ['./session-list.component.scss'],
	animations: [emersionAnimation]
})
export class SessionListComponent implements OnInit {
	
	@Input() sessions: Session[];
	@Input() user: IUser;
	
	@Output() sessionChosen = new EventEmitter<Session>();
	
	constructor() {
	}
	
	ngOnInit() {
	}
	
	joinSession(session: Session) {
		this.sessionChosen.emit(session);
	}
}
