import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { UsersService } from '../../services/users.service';
import { IUser, mapRole } from '../../models/user.models';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { UserProfileComponent } from '../user-profile/user-profile.component';
import { filter } from 'rxjs/operators';
import { AuthService } from '../auth/auth.service';

@Component({
	selector: 'app-users-list',
	templateUrl: './users-list.component.html',
	styleUrls: ['./users-list.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UsersListComponent implements OnInit {
	
	displayedColumns: string[] = ['select', 'id', 'name', 'email', 'role'];
	dataSource: MatTableDataSource<IUser>;
	selection = new SelectionModel<IUser>(true, []);
	mapRole = mapRole;
	
	constructor(
		private usersService: UsersService,
		private cdr: ChangeDetectorRef,
		private popupService: MatDialog,
		private authService: AuthService,
	) {}
	
	async ngOnInit() {
		this.loadUsers();
	}
	
	isAllSelected() {
		const numSelected = this.selection.selected.length;
		const numRows = this.dataSource.data.length;
		return numSelected === numRows;
	}
	
	masterToggle() {
		this.isAllSelected() ?
			this.selection.clear() :
			this.dataSource.data.forEach(row => {
				this.selection.select(row);
			});
	}
	
	checkboxLabel(row?: IUser): string {
		if (!row) {
			return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
		}
		return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
	}
	
	onUserClick(user: IUser) {
		this.popupService
			.open(UserProfileComponent, {
				data: { editable: true, user: user },
			})
			.afterClosed()
			.pipe(filter(Boolean))
			.subscribe(async (data: Partial<IUser>) => {
				const updatedUser = await this.usersService.updateUser(data).toPromise();
				if (data.id === this.authService.user.id) {
					this.authService.updateUser(updatedUser);
				}
				this.loadUsers();
			});
	}
	
	async loadUsers() {
		const users = await this.usersService.requestUsers().toPromise();
		this.dataSource = new MatTableDataSource<IUser>(users);
		this.cdr.detectChanges();
	}
}
