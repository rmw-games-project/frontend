import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';
import { IGame } from '../features/game-wrapper/game.interfaces';
import { shareReplay } from 'rxjs/operators';


@Injectable()
export class GamesService {
	
	private games$: Observable<IGame[]>;
	
	constructor(
		private apiService: ApiService,
	) {}
	
	getGames(needToRefresh = false): Observable<IGame[]> {
		if (needToRefresh) {
			this.games$ = null;
		}
		if (!this.games$) {
			this.games$ = this.requestGames().pipe(
				shareReplay(1)
			);
		}
		return this.games$;
	}
	
	requestGame(gameName: string): Observable<IGame> {
		return this.apiService.get('games/' + gameName);
	}
	
	private requestGames(): Observable<IGame[]> {
		return this.apiService.get('games');
	}
}
