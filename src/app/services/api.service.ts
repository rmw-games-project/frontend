import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
	providedIn: 'root',
})
export class ApiService {
	
	readonly baseUrs = 'http://localhost:5000/api/';
	
	constructor(
		private http: HttpClient,
	) {}
	
	get(url: string): Observable<any> {
		return this.http.get(this.baseUrs + url);
	}
	
	post(url: string, data: any): Observable<any> {
		return this.http.post(this.baseUrs + url, data);
	}
	
	put(url: string, data: any): Observable<any> {
		return this.http.put(this.baseUrs + url, data);
	}
}
