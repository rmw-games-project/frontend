import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { AuthService } from '../features/auth/auth.service';
import { Injectable } from '@angular/core';

@Injectable()
export class HttpErrorHandler implements HttpInterceptor {
	
	constructor(
		private authService: AuthService,
	) {}
	
	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		return next.handle(req).pipe(
			catchError((er) => {
				if (er instanceof HttpErrorResponse) {
					if (er.status === 401) {
						this.authService.logout();
					}
				}
				return of(er);
			}),
		);
	}
}
