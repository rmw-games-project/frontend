import { Injectable } from '@angular/core';
import { AuthService } from '../features/auth/auth.service';
import { HubConnection, HubConnectionBuilder } from '@microsoft/signalr';
import { ApiService } from './api.service';
import { EventRegistrator, SignalRCommonMethodName, SignalREventCallback, SignalREventRegistrators } from '../models/signalR.models';


@Injectable({ providedIn: 'root' })
export class SignalRService {
	
	constructor(
		private authService: AuthService,
		private apiService: ApiService,
	) {
		// connection.on('Receive', data => {
		// 	console.log('data:', data);
		// });
		//
		// connection.onclose(() => {
		// 	console.log('CONNECTION CLOSED!');
		// });
		//
		// connection.start()
		// 	.then(() => connection.invoke('Send', 'Hello', 'LOLsho'));
	}
	
	connect(url: string, cb: (connection: HubConnection) => void): SignalREventRegistrators {
		if (!this.authService.isLoggedIn) {
			throw new Error('Нельзя соединиться по SignalR пока пользователь не зологинен');
		}
		const connection = new HubConnectionBuilder()
			.withUrl(this.apiService.baseUrs + url, {
				accessTokenFactory: this.authService.getToken
			})
			.build();
		
		connection.start();
		
		cb(connection);
		return this.getEventRegistrators(connection);
	}
	
	registerEvent(name: string, connection: HubConnection, callback: SignalREventCallback) {
		connection.on(name, (value: any) => {
			callback(value);
		});
	}
	
	private getEventRegistrator(
		connection: HubConnection,
		methodName: SignalRCommonMethodName
	): EventRegistrator {
		return (callback: SignalREventCallback) => {
			connection.on(methodName, (value: any) => {
				callback(value);
			});
			return this.getEventRegistrators(connection);
		};
	}
	
	private getEventRegistrators(connection: HubConnection): SignalREventRegistrators {
		return {
			onConnected: this.getEventRegistrator(connection, 'onConnected'),
			onMessage: this.getEventRegistrator(connection, 'onMessage'),
			onError: this.getEventRegistrator(connection, 'onError'),
			onDisconnected: this.getEventRegistrator(connection, 'onDisconnected'),
		};
	}
}
