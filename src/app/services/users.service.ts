import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';
import { IUser } from '../models/user.models';


@Injectable({
	providedIn: 'root',
})
export class UsersService {
	
	constructor(
		private apiService: ApiService,
	) {}
	
	requestUsers(): Observable<IUser[]> {
		return this.apiService.get('users');
	}
	
	requestUser(id: number): Observable<IUser> {
		return this.apiService.get('users/' + id);
	}
	
	updateUser(user: Partial<IUser>): Observable<IUser> {
		return this.apiService.put('users', user);
	}
	
	updatePhoto(userId: number, file: File): Observable<void> {
		const formData: FormData = new FormData();
		formData.append('File', file, file.name);
		return this.apiService.put(`users/${userId}/photo`, formData);
	}
}
