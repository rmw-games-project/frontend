import { ChangeDetectionStrategy, Component } from '@angular/core';
import { emersionAnimation } from './animations/emersion.animation';
import { AuthService } from './features/auth/auth.service';
import { Observable } from 'rxjs';
import { filter, mapTo } from 'rxjs/operators';
import { Role } from './models/user.models';
import { SignalRService } from './services/signalR.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	animations: [emersionAnimation],
})
export class AppComponent {
	
	isLoggedInChecked$: Observable<boolean> = this.authService
		.isLoggedIn$.pipe(
			filter((isLoggedIn) => isLoggedIn !== null),
			mapTo(true),
		);
	
	constructor(
		private authService: AuthService,
		private signalRService: SignalRService,
	) {
		this.authService.checkIsLoggedIn();
	}
	
	get showUsersList(): boolean {
		return this.authService.isLoggedIn &&
			this.authService.user.role === Role.Admin;
	}
	
	get showChat(): boolean {
		return this.authService.isLoggedIn;
	}
}
